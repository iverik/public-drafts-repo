Level of Technical Competence Definitions, used for user test participant categorization.

___

See the bottom (Notes) for additional explanations for:
- "Transitional" levels of competence
- "Philosophical", or "soft", computer knowledge


___

## Level 1 - Low
Level 1 is the very basic bar for interacting with a computer in a somewhat productive manner. It works, but that's it.

- Technology is mostly used for entertainment purposes.
- Uses almost exclusively mobile devices (phone, tablet).
- Uses a computer almost exclusively for low-level web content ( e.g. banking, social media).
- Uses very few, if any, computer applications outside of the ones pre-installed and shipped with the computer. 
- Mostly uses closed eco-systems like those provided by Apple, Google and Microsoft, if at all.
- Uses the computer almost 100% as it was set up from the vendor.
- Little to no knowledge of what exists beyone what came pre-installed on their phone or laptop.
- Liable to respond to phising e-mails.
- Have no significant interest in technology, outside what society "demands".
- Consumers of technology, but with little to no understanding of it.
- Can follow simple instructions for doing somewhat advanced tasks.
- Would not be bothered by having to close malware windows launched at every computer start-up; would just close them and continue on with their stuff.

Stereotypes:
- Teen (13-19) or [young-adult](https://en.wikipedia.org/wiki/Young_adult) (18-39) who "lives" on their phone.
- Grandpa/grandma, who mostly looks at pictures of grandkids on social media.
- Students in non-technical fields of study.



## Level 2 - Medium
Level 2 are moderate users of technology, who can use a computer productively and capable og some advanced use. You're likely to unconsciously accept and adapt to a prescribed cookie-cutter workflow, even if it's suboptimal for you.

- Have installed and configured applications that didn't come pre-installed.
- Plays games via third-party software, like Steam, Battle.net and Epic Games.
- Have a basic knowledge of the settings available on the computer (i.e. can find the settings pages).
- May have changed a few settings, but nothing "advanced".
- Are comfortable (even if reluctant) with using a desktop computer and navigating between several types of applications and around the system.
- May even have overclock hardware via applications like MSI Afterburner.
- Unlikely to respons to phising and other attempts at scamming.
- Able to solve somewhat advanced problems on their own. Able to find and integrate information.

Stereotypes:
- The happy and smiling middle-aged user, who have no significant skills or knowledge in technology, but "enjoy using it".
- Students in technical fields of study.
- Office workers.
- Gamers.
- [Script kiddies](https://en.wikipedia.org/wiki/Script_kiddie)


## Level 3 - High
Level 3 users are very adept at using computers and technology to suit their own needs, instead of adapting to the system's and/or application's definition of what the "correct" workflow is. Cookie-cutter doesn't cut it anymore. High technical, theoretical and "philosophical" knowledge.

- Are comfortable with altering advances configs, like BIOS or UEFI settings.
- Can assemble a computer themselves.
- Have very good knowledge of the settings available in their daily-driver operating system.
- Have installed an operating system on a computer via USB or disc images.
- Have at least some basic knowledge of computer programming.
- Able to comfortably use a terminal to perform tasks on their own computer, as well as remotes.
- Have tinkered with, and can set up, niché devices like Raspberry Pi as e.g. network-wide adblocker or media server.
- Have built software from source, outside an IDE.
- Knows what a "kernel" refers to, in computer terms.
- Tinkers with computers and/or other electronics on their spare time.
- Knowledge of technological and design concepts and functionality:
   - A wide and at least general knowledge of concepts like how VMs, online tracking, display servers, window managers and network routers work.
   - Technical insight into both old and new technology.
   - Aware of research about both the positive and the negative ways in which technology affects humans, both as indiviuals and as a society.
   - Aware of how interaction design and software design can be, and are, used for both good and nefarious purposes.
- You entered vim and didn't panic.

Stereotypes:
- Nerds.
- Programmers.
- Computer and technology enthusiasts.
- Electronics Rights and Regulation advocates.


## Level 4 - Very High
Level 4 are experts at using their computer and adapting it to suit their own needs and demands. High level of understanding of inner workings of hardware and software, both technical and theoretical. "Philosophical" knowledge may vary, but likely high.

- Have used "alternative" operating systems like Linux, possibly even BSD and Haiku, over an extended period of time. Not neccessarily have it as their daily driver, but have good knowledge of at least Linux.
- Have good knowledge of, and long experience with, all the three major operating systems (Mac OS, Windows and Linux).
- Would be completely comfortable with having to use the terminal instead of the GUI for navigating around the operating system and performing file handling, editing configs, as well as more advanced tasks.
- Have set up servers, e.g. NAS.
- Have compiled custom drivers.
- Have good knowledge of the interal workings of a computer, both software and hardware.
- Can use SSH to remote into "dead" network equipment and repair it.
- High-level electronics tinkerer.
- You were able to exit vim without using the computer's off-button.

Stereotypes:
- Nerd enthusiasts.
- Full-stack developers.
- Old-school computer programmers.



# Notes

### A note on "Transitional" Levels of Competence

- Some users are categorized as "someLevel to otherLevel". 
- This is because not all fall squarely in one category or the other.
- We asked the test particiants to rank themselves, where some gave a response like "well, probably low to medium, because such-and-such". 
- This a very common response, which we think most people would find very familiar.
- Even if our Levels have been fairly broadly, yet narrowly, specified, it is always hard to put someone in exactly one category.
- Thus, to allow for the fact that people aren't made to fit "square holes", we chose to give them the categorization they saw fit for themselves, as long as they were able to give a plausible reason as to why they felt that particular way.


### A note on "Philosophical" Computer Knowledge

Philosophical, or "soft", knowledge refers to idiology, theoretical debates, moral and ethical questions, with regards to computers, software, IT and technology in general.

How best to design an application? Will it benefit, or harm, its users? Will we knowingly implement deceitful design patterns? Will we trick unsuspecting users to give up personal information? Nudge them to buy products they don't need? Play on their emotions to generate "engagement" on social media? Will we invest in supercomputers to calculate protein folding or climate models, or is it much better to spend the money on a server-farm for 30 second video clips designed to keep people glued to their screens? How serious should we take AI and the Alignment Problem?

In terms of computer's and computer science, there is no offical brach called "computer philosphy", even if there are people who are just that. People like [Jaron Lanier](https://en.wikipedia.org/wiki/Jaron_Lanier) and [Richard Stallman](https://en.wikipedia.org/wiki/Richard_Stallman) are vocal advocates of computers and software working *for* humands, instead of the notion that humans should adapt to suit the limitations (real and by-design) of computers and software. The title of one of Lanier's books, "You are not a gadget", sums up this view nicely. Additionally, scientists like Nick Bostrom are leading the debate on AI, and their potential dangers. Many more have expressed their misgivings about how Social Media is specifically designed to play on, and amplify, the worst in people.

These are all tpoics that anyone with a Level 3 (High) or Level 4 (Very High) should have at least a decent amount of knowledge about.





